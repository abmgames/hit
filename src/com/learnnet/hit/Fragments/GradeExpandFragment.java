package com.learnnet.hit.Fragments;

import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.learnnet.hit.R;
import com.learnnet.hit.Adapters.GradeDetailsAdapter;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Objects.GradeData;
import com.learnnet.hit.Objects.GradeDetailsData;
import com.learnnet.hit.Utils.GlobalDefs;
import com.learnnet.hit.activity.Grades;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class GradeExpandFragment extends Fragment implements MyInterface, GlobalDefs {

	// =================================================
	// FIELDS
	// =================================================

	private TextView credits;
	private TextView gradeFinal;
	private GradeData mCurrentGradeData;
	private List<GradeDetailsData> mGradeDetailsDataList;
	private ListView listView;
	private GradeDetailsAdapter gradeDetailsAdapter;
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		this.mCurrentGradeData = ((Grades)getActivity()).getCurrectGradeData();
		this.mGradeDetailsDataList = ((Grades)getActivity()).getGradeDetailsData();
		
		View v = (View) inflater.inflate(R.layout.fragment_grade_expand, container, false);
		credits = (TextView) v.findViewById(R.id.tvGradeCredits);
		gradeFinal = (TextView) v.findViewById(R.id.tvGradeFinal);
		credits.setText(mCurrentGradeData.credits);
		gradeFinal.setText(mCurrentGradeData.finalGrade);
		
		listView = (ListView) v.findViewById(R.id.lvGradeDetails);
		gradeDetailsAdapter = new GradeDetailsAdapter((Grades)getActivity(), R.layout.grade_details_list_item, mGradeDetailsDataList);
		listView.setAdapter(gradeDetailsAdapter);
		
		return v;
	}

	@Override
	public boolean isNetworkConnected() {
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	

	// =================================================
	// CLASS LOGIC
	// =================================================

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	// =================================================
	// INNER CLASSES
	// =================================================
	
}
