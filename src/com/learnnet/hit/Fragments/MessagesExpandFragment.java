package com.learnnet.hit.Fragments;

import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.learnnet.hit.R;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Utils.GlobalDefs;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MessagesExpandFragment extends Fragment implements MyInterface, GlobalDefs {

	// =================================================
	// FIELDS
	// =================================================

	private TextView mMessage;
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	

	// =================================================
	// CLASS LOGIC
	// =================================================

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Bundle b = getArguments();
		String mHtml = b.getString(BUNDLE_MESSAGE_HTML);
		View v = (View) inflater.inflate(R.layout.fragment_message_expand, container, false);
		mMessage = (TextView) v.findViewById(R.id.tvHtml);
		mMessage.setText(Html.fromHtml(mHtml));
	    mMessage.setMovementMethod(LinkMovementMethod.getInstance());
		return v;
	}

	@Override
	public boolean isNetworkConnected() {
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	// =================================================
	// INNER CLASSES
	// =================================================

}