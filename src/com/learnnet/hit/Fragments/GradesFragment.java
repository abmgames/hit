package com.learnnet.hit.Fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.learnnet.hit.MyApplication;
import com.learnnet.hit.R;
import com.learnnet.hit.Adapters.GradesAdapter;
import com.learnnet.hit.Communication.MyMichlolRequests;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Objects.GradeData;
import com.learnnet.hit.Utils.GlobalDefs;
import com.learnnet.hit.Utils.xmlParser;
import com.learnnet.hit.activity.Grades;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class GradesFragment extends Fragment implements GlobalDefs, MyInterface, OnItemSelectedListener, OnItemClickListener {
	
	// =================================================
	// FIELDS
	// =================================================
	
	private ListView mGradesListView;
	private GradesAdapter mGradesAdapter;
	private List<GradeData> mData;
	private MyApplication mApp;
	private SpinnerAdapter mSpinnerAdapter;
	private Spinner spinner;
	private ArrayList<String> studentYears;
	
	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_grades, container, false);
		
		mGradesListView = (ListView) v.findViewById(R.id.lv_grades);
		
		mApp = ((Grades)getActivity()).getMyApp();
		
		mData = ((Grades)getActivity()).getGradesData();
		
		mGradesListView.setOnItemClickListener(this);
		
		studentYears = new ArrayList<String>();
		for (int i=0 ; i<mApp.mCurrentUserYears.size() ; i++) {
			studentYears.add(mApp.mCurrentUserYears.get(i).getYear());
		}

		// Set up spinner
		spinner = (Spinner) v.findViewById(R.id.spinner1);
		mSpinnerAdapter = ArrayAdapter.createFromResource((Grades)getActivity(), R.array.my_array_exams_menu,
				android.R.layout.simple_spinner_dropdown_item);
		mSpinnerAdapter = new ArrayAdapter<String>((Grades)getActivity(), android.R.layout.simple_spinner_dropdown_item, studentYears);

		spinner.setAdapter(mSpinnerAdapter);
		spinner.setOnItemSelectedListener(this);

		return v;
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		String year = mApp.mCurrentUserYears.get(arg2).getYear();
		if (MyApplication.isInternetAvailable) {
			new GetGradesTask().execute(year);
		}
		else {
			mApp.showNoInternetDialog((Grades)getActivity());
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		((Grades)getActivity()).getGradeDetails(mData.get(arg2));
	}
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	
	
	// =================================================
	// CLASS LOGIC
	// =================================================
	
	private String michlolRequestGrades(String username, String url, String year) {
		MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();
		return michlolMessagesRequest.call(REQ_GRADES, username, "UnsedValueJustForChecks",
				year, null, url, false); // false doesn't matter here.
	}
	
	private void showList() {
		mGradesAdapter = new GradesAdapter((Grades)getActivity(), R.layout.grade_list_item, mData);
		mGradesListView.setAdapter(mGradesAdapter);
		mGradesAdapter.notifyDataSetChanged();
	}
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	// =================================================
	// INNER CLASSES
	// =================================================
	
	private class GetGradesTask extends AsyncTask<String, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((Grades)getActivity()).displaySpinner();
		}
		
		@Override
		protected Void doInBackground(String... arg0) {
			StringBuilder sb = new StringBuilder();
			
			String username = mApp.mPrefs.getString(PREF_USER_NAME, "");
			String url = mApp.mPrefs.getString(PREF_MICHLOL_URL, "");
			
			sb.append(michlolRequestGrades(username, url, arg0[0])); // arg0[0] = mApp.mCurrentUserYears.get(0).getYear()
			
			xmlParser xp = new xmlParser();
			mData = xp.parseGradesData(sb.toString());

			Collections.sort(mData);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			((Grades)getActivity()).setGradesData(mData);
			showList();
			((Grades)getActivity()).hideSpinner();
		}
	 }
}
