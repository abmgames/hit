package com.learnnet.hit.Fragments;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
import java.util.Map;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.learnnet.hit.MyApplication;
import com.learnnet.hit.R;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Utils.GlobalDefs;
import com.learnnet.hit.activity.MainMenu;

public class FacebookFragment extends Fragment implements MyInterface, GlobalDefs {
	
	// =================================================
	// FIELDS
	// =================================================

	private WebView webView;
	private ProgressBar mProgressBar;
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	
	
	// =================================================
	// CLASS LOGIC
	// =================================================
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Bundle b = getArguments();
		String mUrl = b.getString(BUNDLE_URL);
		View mainView = (View) inflater.inflate(R.layout.web_view_layout, container, false);
		mProgressBar = (ProgressBar) mainView.findViewById(R.id.pbWebSpinner);
		this.webView = (WebView) mainView.findViewById(R.id.webview);
		this.webView.setWebViewClient(new MyWebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				view.setVisibility(View.VISIBLE);
				mProgressBar.setVisibility(View.INVISIBLE);
			}
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				mProgressBar.setVisibility(View.VISIBLE);
			}
		});
		this.webView.getSettings().setBuiltInZoomControls(false); 
		this.webView.getSettings().setSupportZoom(false);
		this.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);   
		this.webView.getSettings().setAllowFileAccess(true); 
		this.webView.getSettings().setDomStorageEnabled(true);
		this.webView.loadUrl(mUrl);
		return mainView;
	}

	@Override
	public boolean isNetworkConnected() {
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		MyApplication mApp = ((MainMenu)getActivity()).getMyApp();
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			return (String) mApp.mPropertiesSpecific.get(key);
		}
		else {
			return null;
		}
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	public WebView getWebView() {
		return this.webView;
	}
	
	// =================================================
	// INNER CLASSES
	// =================================================
	
	public class MyWebViewClient extends WebViewClient {        
		/* (non-Java doc)
		 * @see android.webkit.WebViewClient#shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String)
		 */

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.endsWith(".mp4")) 
			{
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse(url), "video/*");

				view.getContext().startActivity(intent);
				return true;
			} 
			else {
				return super.shouldOverrideUrlLoading(view, url);
			}
		}
	}
}
