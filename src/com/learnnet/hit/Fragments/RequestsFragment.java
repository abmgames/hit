package com.learnnet.hit.Fragments;

import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.learnnet.hit.MyApplication;
import com.learnnet.hit.R;
import com.learnnet.hit.Adapters.RequestsAdapter;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Objects.RequestData;
import com.learnnet.hit.Utils.GlobalDefs;
import com.learnnet.hit.activity.Requests;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class RequestsFragment extends Fragment implements GlobalDefs, MyInterface {

	private ListView mRequestListView;
	private RequestsAdapter mRequestsAdapter;
	private List<RequestData> mData;
	private MyApplication mApp;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_requests, container, false);
		
		mRequestListView = (ListView) v.findViewById(R.id.lv_requests);
		
		mApp = ((Requests)getActivity()).getMyApp();
		
		mData = ((Requests)getActivity()).getRequestsData();
		
		mRequestsAdapter = new RequestsAdapter((Requests)getActivity(), R.layout.request_list_item, mData);
		mRequestListView.setAdapter(mRequestsAdapter);

		return v;
	}

	@Override
	public boolean isNetworkConnected() {
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}
}
