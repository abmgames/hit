package com.learnnet.hit.Fragments;

import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.learnnet.hit.MyApplication;
import com.learnnet.hit.R;
import com.learnnet.hit.Adapters.MessageAdapter;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Objects.MessageData;
import com.learnnet.hit.Utils.GlobalDefs;
import com.learnnet.hit.activity.Messages;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MessagesFragment extends Fragment implements GlobalDefs, OnItemClickListener, MyInterface {

	private ListView mMessageListView;
	private MessageAdapter mMessageAdapter;
	private List<MessageData> mData;
	private MyApplication mApp;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_messages, container, false);
		
		mMessageListView = (ListView) v.findViewById(R.id.lv_messages);
		
		mApp = ((Messages)getActivity()).getMyApp();
		
		mData = ((Messages)getActivity()).getMessagesData();
		
		mMessageListView.setOnItemClickListener(this);
		
		mMessageAdapter = new MessageAdapter((Messages)getActivity(), R.layout.messages_list_item, mData);
		mMessageListView.setAdapter(mMessageAdapter);

		return v;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		((Messages)getActivity()).openMessage(mData.get(arg2));
	}

	@Override
	public boolean isNetworkConnected() {
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

}