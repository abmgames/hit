package com.learnnet.hit.Fragments;

import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.learnnet.hit.MyApplication;
import com.learnnet.hit.R;
import com.learnnet.hit.Adapters.ExamsAdapter;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Objects.ExamData;
import com.learnnet.hit.Utils.GlobalDefs;
import com.learnnet.hit.activity.Exams;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class ExamsFragment extends Fragment implements GlobalDefs, OnItemClickListener, MyInterface, OnItemSelectedListener {

	// =================================================
	// FIELDS
	// =================================================

	private ListView mExamsListView;
	private ExamsAdapter mExamsAdapter;
	private List<ExamData> mData;
	private MyApplication mApp;
	private SpinnerAdapter mSpinnerAdapter;
	private Spinner spinner;

	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_exams, container, false);

		mExamsListView = (ListView) v.findViewById(R.id.lv_exams);

		mApp = ((Exams)getActivity()).getMyApp();

//		if (!MyApplication.isAppKilled) {

			mData = ((Exams)getActivity()).getExamsData(0);

			mExamsListView.setOnItemClickListener(this);

			// Set up spinner
			spinner = (Spinner) v.findViewById(R.id.spinner1);
			mSpinnerAdapter = ArrayAdapter.createFromResource((Exams)getActivity(), R.array.my_array_exams_menu,
					android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(mSpinnerAdapter);
			spinner.setOnItemSelectedListener(this);
			spinner.setSelection(((Exams)getActivity()).getSpinnerIndex());

			mExamsAdapter = new ExamsAdapter((Exams)getActivity(), R.layout.exam_list_item, mData);
			mExamsListView.setAdapter(mExamsAdapter);

			return v;
//		}
//		else {
//			return v;
//		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		((Exams)getActivity()).openExam(mData.get(arg2));
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		mData = ((Exams)getActivity()).getExamsData(arg2);
		((Exams)getActivity()).setSpinnerIndex(arg2);
		mExamsAdapter = new ExamsAdapter((Exams)getActivity(), R.layout.exam_list_item, mData);
		mExamsListView.setAdapter(mExamsAdapter);
		mExamsAdapter.notifyDataSetChanged();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	

	// =================================================
	// CLASS LOGIC
	// =================================================

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	// =================================================
	// INNER CLASSES
	// =================================================

}
