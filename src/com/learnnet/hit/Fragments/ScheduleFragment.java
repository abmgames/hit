package com.learnnet.hit.Fragments;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;

import com.learnnet.hit.MyApplication;
import com.learnnet.hit.R;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Utils.GlobalDefs;
import com.learnnet.hit.activity.Schedule;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class ScheduleFragment extends Fragment implements GlobalDefs, MyInterface {
	
	// =================================================
	// FIELDS
	// =================================================
	
	private ExpandableListView mScheduleExpandableListView;
	private MyApplication mApp;
	private ExpandableListAdapter adapterExpandable;
	private Button btn;
	
	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_schedule, container, false);
		
		mScheduleExpandableListView = (ExpandableListView) v.findViewById(R.id.lv_schedule);
		
		mApp = ((Schedule)getActivity()).getMyApp();
		
		setList();

		return v;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		btn = (Button) view.findViewById(R.id.btn);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				openDatePicker();
			}
		});
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	
	
	// =================================================
	// CLASS LOGIC
	// =================================================
	
	private void setList() {

		ArrayList<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
		groupData = ((Schedule)getActivity()).getGroupData();
		ArrayList<List<Map<String, String>>> childrenData = new ArrayList<List<Map<String, String>>>();
		childrenData = ((Schedule)getActivity()).getChildrenData();
		adapterExpandable = new SimpleExpandableListAdapter((Schedule)getActivity(), groupData,
				R.layout.expandable_list_item, new String[] { "category" },
				new int[] { R.id.header }, childrenData, R.layout.schedule_list_item,
				new String[] { "line1", "line2", "line3", "line4" }, new int[] { R.id.line1,
						R.id.line2, R.id.line3, R.id.line4 });
		
		mScheduleExpandableListView.setAdapter(adapterExpandable);
		for (int i=0 ; i<groupData.size() ; i++) {
			mScheduleExpandableListView.expandGroup(i);
		}
	}
	
	private void openDatePicker() {
		final Calendar c = Calendar.getInstance();
		int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        OnDateSetListener odsl = new OnDateSetListener() {
			
			@Override
			public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
				if (MyApplication.isInternetAvailable) {
					((Schedule)getActivity()).setDate(arg1, arg2, arg3);
				}
				else {
					mApp.showNoInternetDialog((Schedule)getActivity());
				}
			}
		};
        
        DatePickerDialog dpdFromDate = new DatePickerDialog(getActivity(), odsl, mYear, mMonth,
                  mDay);
        dpdFromDate.show();

        dpdFromDate.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
               if (which == DialogInterface.BUTTON_NEGATIVE) {
                    //et_to_date.setText("");
               }
            }
        });
	}
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	// =================================================
	// INNER CLASSES
	// =================================================

}