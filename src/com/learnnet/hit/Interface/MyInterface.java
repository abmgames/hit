package com.learnnet.hit.Interface;

import java.util.Map;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public interface MyInterface {

	boolean isNetworkConnected();
	
	String getPropertiesString(String key);
	int getPropertiesInt(String key);
	boolean getPropertiesBoolean(String key);
	Map<String, Object> getPropertiesHashTable(String key);
	
}
