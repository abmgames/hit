package com.learnnet.hit.Handlers;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.learnnet.hit.Objects.TeacherScheduleData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class TeacherScheduleHandler extends DefaultHandler{

	List<TeacherScheduleData> messages;
	private TeacherScheduleData currentMessage;
	
	
	private StringBuilder builder;
	
	public List<TeacherScheduleData> getMessages(){
        return this.messages;
    }
	
	@Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }
	
	@Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);
        
            if (localName.equalsIgnoreCase("MESHAA"))
            {
            	currentMessage.BEGINTIME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("ADSHAA"))
            {
            	currentMessage.ENDTIME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("COURSE_NAME"))
            {
            	currentMessage.COURSENAME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("NUMBER_OF_STUDENTS"))
            {
            	currentMessage.NUMOFSTUDENTS=builder.toString();
            }
            else if (localName.equalsIgnoreCase("YOM"))
            {
            	currentMessage.DAY=builder.toString();
            }
            else if (localName.equalsIgnoreCase("LOCATION"))
            {
            	currentMessage.LOCATION=builder.toString();
            }
            else if (localName.equalsIgnoreCase("RECORD"))
            {
            	messages.add(currentMessage);
            }
            builder.setLength(0);    
        
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        messages = new ArrayList<TeacherScheduleData>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
            Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("RECORD")){
            this.currentMessage = new TeacherScheduleData();
        }
    }
	
}
