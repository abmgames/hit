package com.learnnet.hit.Handlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.learnnet.hit.Objects.IdmLoginData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class IdmLoginDataHandler extends DefaultHandler {

	private IdmLoginData data;

	private StringBuilder builder;

	public IdmLoginData getMessages() {

		return this.data;

	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		builder.append(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String name)
			throws SAXException {
		super.endElement(uri, localName, name);
		if (this.data.WorkforceID != null) {
			Log.e("localName:", builder.toString());
			if (localName.equalsIgnoreCase("WorkforceID")) {
				data.WorkforceID = builder.toString();
			} else if (localName.equalsIgnoreCase("retCode")) {
				data.retCode = builder.toString();
			} else if (localName.equalsIgnoreCase("MichlolUserName")) {
				data.MichlolUserName = builder.toString();
			} else if (localName.equalsIgnoreCase("tZehut")) {
				data.tZehut = builder.toString();
			} else if (localName.equalsIgnoreCase("ASHMobileAllow")) {
				data.ASHMobileAllow = builder.toString();
			}
			builder.setLength(0);
		}
	}

	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		builder = new StringBuilder();
	}

	@Override
	public void startElement(String uri, String localName, String name,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, name, attributes);
		if (localName.equalsIgnoreCase("authResponse")) {
			this.data = new IdmLoginData();

		}
	}
	
}
