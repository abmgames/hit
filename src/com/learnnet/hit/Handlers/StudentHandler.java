package com.learnnet.hit.Handlers;

import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.learnnet.hit.Objects.StudentData;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class StudentHandler extends DefaultHandler {

    LinkedList<StudentData> messages;
    private StudentData currentMessage;

    private StringBuilder builder;

    public LinkedList<StudentData> getMessages() {
        return this.messages;
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);

        boolean isRecordElement = localName.equalsIgnoreCase("RECORD");

        if (localName.equalsIgnoreCase("STUDENTNAME")) {
            currentMessage.STUDENTNAME = builder.toString();
        } else if (localName.equalsIgnoreCase("STUDENTID")) {
            currentMessage.STUDENTID = builder.toString();
        } else if (localName.equalsIgnoreCase("STUDENTACADEMICYEAR")) {
            currentMessage.STUDENTACADEMICYEAR = builder.toString();
        } else if (localName.equalsIgnoreCase("STUDENTINFO")) {
            currentMessage.STUDENTINFO = builder.toString();
        } else if (localName.equalsIgnoreCase("ADMISSION_STATUS")) {
            currentMessage.ADMISSION_STATUS = builder.toString();
        } else if (localName.equalsIgnoreCase("IMAGEBASE64"))
        {
            try{
                byte[] decodedString = Base64.decode(builder.toString(), Base64.DEFAULT);
                currentMessage.IMAGE = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);;
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }


        if(isRecordElement)
            messages.add(currentMessage);

        builder.setLength(0);

    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        messages = new LinkedList<StudentData>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
                             Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("RECORD")) {
            this.currentMessage = new StudentData();
        }
    }
}