package com.learnnet.hit.Handlers;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.learnnet.hit.Objects.UserDetailsData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class UserDetailsHandler extends DefaultHandler {

	private UserDetailsData data;
	private List<UserDetailsData> allData = new ArrayList<UserDetailsData>();

	private StringBuilder builder;

	public UserDetailsData getMessages() {

		if(allData.size() > 0){
			return allData.get(0);
		}else{
			return new UserDetailsData();
		}

	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		builder.append(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String name)
			throws SAXException {
		super.endElement(uri, localName, name);

		if (localName.equalsIgnoreCase("FULLNAME")) {
			data.FULLNAME = builder.toString().replace("__AMP__", "&");
		}
		else if (localName.equalsIgnoreCase("TOKEN")) {
			data.TOKEN = builder.toString().replace("__AMP__",
					"&");
		}
		else if (localName.equalsIgnoreCase("STUDENT_ACADEMIC_YEAR")) {
			data.STUDENT_ACADEMIC_YEAR = builder.toString().replace("__AMP__",
					"&");
		}
		else if (localName.equalsIgnoreCase("LOGIN_STATUS")) {
			data.LOGIN_STATUS = builder.toString().replace("__AMP__", "&");
		}
		else if (localName.equalsIgnoreCase("LOGIN_STATUS_TEXT")) {
			data.LOGIN_STATUS_TEXT = builder.toString().replace("__AMP__", "&");
		}
		else if (localName.equalsIgnoreCase("STUDENT_SPECIALITY")) {
			data.STUDENT_SPECIALITY = builder.toString().replace("__AMP__",
					"&");
		}
		else if (localName.equalsIgnoreCase("STUDENT_EMAIL")) {
			data.STUDENT_EMAIL = builder.toString().replace("__AMP__", "&");
		}
		else if (localName.equalsIgnoreCase("STUDENT_STATUS")) {
			data.STUDENT_STATUS = builder.toString().replace("__AMP__", "&");

		}
		else if (localName.equalsIgnoreCase("STUDENT_DEPARTMENT")) {
			data.STUDENT_DEPARTMENT = builder.toString().replace("__AMP__", "&");
		}
		else if (localName.equalsIgnoreCase("TEACHER_ID")) {
			data.TEACHER_ID = builder.toString().replace("__AMP__", "&");
		}
		else if (localName.equalsIgnoreCase("STUDENT_ADDRESS")) {
			data.STUDENT_ADDRESS = builder.toString().replace("__AMP__", "&");
		}
		else if (localName.equalsIgnoreCase("STUDENT_CELLULARPHONE")) {
			data.STUDENT_CELLULARPHONE = builder.toString().replace("__AMP__", "&");
		}
		else if (localName.equalsIgnoreCase("CURRENTYEAR")) {
			data.CURRENTYEAR = builder.toString().replace("__AMP__", "&");
		}
		else if (localName.equalsIgnoreCase("RECORD")) {
			allData.add(data);
		}
		builder.setLength(0);

	}

	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		builder = new StringBuilder();
	}

	@Override
	public void startElement(String uri, String localName, String name,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, name, attributes);
		if (localName.equalsIgnoreCase("RECORD")) {
			this.data = new UserDetailsData();

		}
	}

}
