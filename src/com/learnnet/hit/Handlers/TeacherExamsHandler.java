package com.learnnet.hit.Handlers;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.learnnet.hit.Objects.TeacherExamData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class TeacherExamsHandler extends DefaultHandler {

	List<TeacherExamData> messages;
	private TeacherExamData currentMessage;
	
	private StringBuilder builder;
	
	public List<TeacherExamData> getMessages(){
        return this.messages;
    }
	
	@Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }
	
	@Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);
        
            if (localName.equalsIgnoreCase("COURSE_NUMBER"))
            {
            	currentMessage.COURSENUMBER=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXAM_FIRSTDATE"))
            {
            	currentMessage.FIRSTDATE=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXAM_FIRSTDATE_TIME"))
            {
            	currentMessage.FIRSTTIME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXAM_SECONDDATE"))
            {
            	currentMessage.SECONDDATE=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXAM_SECONDDATE_TIME"))
            {
            	currentMessage.SECONDTIME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXAM_THIRDDATE"))
            {
            	currentMessage.THIRDDATE=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXAM_THIRDDATE_TIME"))
            {
            	currentMessage.THIRDTIME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("RECORD"))
            {
            	messages.add(currentMessage);
            }
            builder.setLength(0);    
        
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        messages = new ArrayList<TeacherExamData>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
            Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("RECORD")){
            this.currentMessage = new TeacherExamData();
        }
    }
	
}
