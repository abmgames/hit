package com.learnnet.hit.Utils;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0.0
 * @since       2013-11-10
 */
public interface GlobalDefs {

	// ===============================================
	// CURRENT INSTITUTE
	// ===============================================
	
//	public final static String INSTITUTE_NAME = "kRuppin";
//	public final static String INSTITUTE_NAME = "kShankar";
	public final static String INSTITUTE_NAME = "kHIT";
//	public final static String INSTITUTE_NAME = "kTelHai";
//	public final static String INSTITUTE_NAME = "kAshkelon";
//	public final static String INSTITUTE_NAME = "kHemdatAdarom";
	
	// ===============================================
	// PLIST : PROPERTIES
	// ===============================================

	public final static String PLIST_PROPERTIES_FILE_LINK = "https://s3-us-west-2.amazonaws.com/learnnet/configuration/properties.plist";
	public final static String PLIST_PROPERTOES_FILE_NAME = "properties.plist";
	
	public final static String PLIST_NULL_VALUE = "\n\t\t";
	
	public final static String PROP_BRANCH_ID = "kBranchID";
	public final static String PROP_INSTITUTE_NAME = "kLoginScreenTitle";
	public final static String PROP_WELCOME_MSG = "kLoginScreenSubTitle";
	public final static String PROP_IDM_URL_PATH = "kIDMUrlPath";
	public final static String PROP_P_KEY = "kAPIxx";
	public final static String PROP_MICHLOL_URL_PATH = "kMichlolUrlPath";
	public final static String PROP_RESTRICTION_LOGIN_STATUS = "kRestricationLoginStatus";
	public final static String PROP_STUDENT_TABLE_FEATURES = "kStudentTableFeatures";
	public final static String PROP_FACEBOOK_URL = "kFacebookURL";
	public final static String PROP_YOUTUBE_URL = "kYoutubeURL";
	public final static String PROP_SUPPORT_MAIL = "kSupportEmail";
	public final static String PROP_FEED_BACK = "kFeedbackURL";
	public final static String PROP_STUD_ASSOC = "kAgoda";
	public final static String PROP_ABOUT = "kAbout";
	public final static String PROP_RESET_PASSWORD = "kResetPassword";
	public final static String PROP_POST_USER_INFO = "kPostUserInfoURL";
	public final static String PROP_ICON_URL = "kBranchIconURL";

	// ===============================================
	// ANALYTIC SCREENS
	// ===============================================

	public final static String ANALYTIC_AVERAGE = "Average";
	public final static String ANALYTIC_FACE_BOOK = "Face Book";
	public final static String ANALYTIC_DROP_BOX = "Drop Box";
	public final static String ANALYTIC_YOU_TUBE = "You Tube";
	
	// ===============================================
	// NAVIGATION DRAWER
	// ===============================================
	
	public final static String DRAWER_TITLE = "Title";
	public final static String DRAWER_IMAGE = "Image";
	
	// ===============================================
	// LIST VIEW ADAPTERS
	// ===============================================
	
	public final static String MENU_TITLE = "menuTitle";
	public final static String MENU_DESC = "menuDesc";
	public final static String MENU_IMG = "menuImg";
	
	public final static String MESSAGE_TITLE = "messageTitle";
	public final static String MESSAGE_DESC = "messageDesc";
	public final static String MESSAGE_IMG = "messageImg";
	
	// ===============================================
	// CONSTANT STRINGS
	// ===============================================
	
	public final static String STR_SUCCESS = "success";
	
	// ===============================================
	// BUNDLE STRINGS
	// ===============================================
	
	public final static String BUNDLE_URL = "url";
	public final static String BUNDLE_MESSAGE_HTML = "html";
	public final static String BUNDLE_EXAM_TITLE = "examTitle";
	public final static String BUNDLE_EXAM_DATE = "examDate";
	public final static String BUNDLE_EXAM_TIME = "examTime";
	public final static String BUNDLE_EXAM_LECTURER = "examLecturer";
	public final static String BUNDLE_EXAM_CLASS = "examClassRoom";
	
	// ===============================================
	// FRAGMENT BACK STACK STRINGS
	// ===============================================
	
	public final static String FRAG_MSG_EXPAND = "msgExpand";
	
	// ===============================================
	// REQUEST ID's
	// ===============================================
	
	public final static String REQ_LOGIN = "48";
	public final static String REQ_MESSAGES = "19";
	public final static String REQ_GRADES = "4";
	public final static String REQ_EXAMS = "28";
	public final static String REQ_YEARS = "37";
	public final static String REQ_SCHEDULE = "27";
	public final static String REQ_LECTURER_SCHEDULE = "52";
	public final static String REQ_AVERAGE = "39";
	public final static String REQ_TEACHER_SCHEDULE = "23";
	public final static String REQ_LIST_PRESENCE = "63";
	public final static String REQ_ADMISSIONS = "64";
	public final static String REQ_TEACHER_EXAMS = "49";
	public final static String REQ_REQUESTS = "32";
	public final static String REQ_GRADE_DETAILS = "36";
	
	// ===============================================
	// MAIL 
	// ===============================================
	
	public final static String MAIL_SUBJECT = "Learnnet support request - Android device";
	public final static String MAIL_BODY = "\n\n\nThis mail has been sent from client branch number";
	public final static String MAIL_DO_NOT_DELETE = " - נא לא למחוק שורה זו";
	
	// ===============================================
	// MAIL 
	// ===============================================
	
	public final static String SURVEY_STATUS = "status";
	public final static String SURVEY_URL = "url";
	public final static int SURVEY_STATUS_OK = 200;
	
	// ===============================================
	// SHARED PREFERENCES
	// ===============================================
	
	public final static String PREF_FILE_NAME = "My Prefs";
	public final static String PREF_USER_NAME = "Username";
	public final static String PREF_PASSWORD = "Password";
	public final static String PREF_MICHLOL_URL = "MichlolUrl";
	public final static String PREF_LOGIN_USER_NAME = "LoginUsername";
	public final static String PREF_LOGIN_PASSWORD = "LoginPassword";

	// ===============================================
	// Server Side
	// ===============================================

	public final static String SERVER_GET_MESSAGES = "http://abmgames.com/__hosted/push/api/getMessage.php";
	public final static String SERVER_DELETE_MESSAGES = "http://abmgames.com/__hosted/push/api/deleteMessage.php";

	// ===============================================
	// LOG & DEBUG
	// ===============================================
	
	public final static String TAG = "LearnNetLog";
	
}
