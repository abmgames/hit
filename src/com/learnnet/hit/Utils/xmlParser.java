package com.learnnet.hit.Utils;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.learnnet.hit.Handlers.AverageHandler;
import com.learnnet.hit.Handlers.ExamHandler;
import com.learnnet.hit.Handlers.GradeDetailsHandler;
import com.learnnet.hit.Handlers.IdmLoginDataHandler;
import com.learnnet.hit.Handlers.MGradesHandler;
import com.learnnet.hit.Handlers.MMessageHandler;
import com.learnnet.hit.Handlers.MYearsHandler;
import com.learnnet.hit.Handlers.OutputDataHandler;
import com.learnnet.hit.Handlers.RequestsHandler;
import com.learnnet.hit.Handlers.ScheduleHandler;
import com.learnnet.hit.Handlers.StudentHandler;
import com.learnnet.hit.Handlers.TeacherExamsHandler;
import com.learnnet.hit.Handlers.TeacherLessonHandler;
import com.learnnet.hit.Handlers.TeacherScheduleHandler;
import com.learnnet.hit.Handlers.UserDetailsHandler;
import com.learnnet.hit.Objects.ExamData;
import com.learnnet.hit.Objects.GradeData;
import com.learnnet.hit.Objects.GradeDetailsData;
import com.learnnet.hit.Objects.IdmLoginData;
import com.learnnet.hit.Objects.MessageData;
import com.learnnet.hit.Objects.RequestData;
import com.learnnet.hit.Objects.ScheduleData;
import com.learnnet.hit.Objects.StudentData;
import com.learnnet.hit.Objects.TeacherExamData;
import com.learnnet.hit.Objects.TeacherLessonData;
import com.learnnet.hit.Objects.TeacherScheduleData;
import com.learnnet.hit.Objects.UserDetailsData;
import com.learnnet.hit.Objects.YearsData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class xmlParser {

	public IdmLoginData parseOutputDataNewLogin(DataInputStream is) {

		IdmLoginDataHandler handler = new IdmLoginDataHandler();
		IdmLoginData result = new IdmLoginData();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			xr.parse(new InputSource(is));

			result = handler.getMessages();
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
	
	public String parseOutputData(DataInputStream is) {

		OutputDataHandler handler = new OutputDataHandler();
		String result = new String();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			xr.parse(new InputSource(is));

			result = handler.getMessages();
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
	
	public UserDetailsData parseUserDetailsData(String content) {

		UserDetailsHandler handler = new UserDetailsHandler();
		UserDetailsData result = new UserDetailsData();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
	
	public List<MessageData> parseMMessageData(String content) {

		MMessageHandler handler = new MMessageHandler();
		List<MessageData> result = new ArrayList<MessageData>();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();
	
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
	
	public List<GradeData> parseGradesData(String content) {

		MGradesHandler handler = new MGradesHandler();
		List<GradeData> result = new ArrayList<GradeData>();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
	
	public List<GradeDetailsData> parseGradeDetailsData(String content) {

		GradeDetailsHandler handler = new GradeDetailsHandler();
		List<GradeDetailsData> result = new ArrayList<GradeDetailsData>();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
	
	public List<YearsData> parseYearsData(String content) {

		MYearsHandler handler = new MYearsHandler();
		List<YearsData> result = new ArrayList<YearsData>();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
	
	public List<ExamData> parseExamData(String content) {

		ExamHandler handler = new ExamHandler();
		List<ExamData> result = new ArrayList<ExamData>();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			content = content.replace("<br>", " ");

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
	
	public List<ScheduleData> parseScheduleData(String content){
		
		ScheduleHandler handler = new ScheduleHandler();
	    List<ScheduleData> result = new ArrayList<ScheduleData>();
	    XMLReader xr;
	    
	    SAXParserFactory spf = SAXParserFactory.newInstance();
	    
	    
	    try {
	    	SAXParser sp = spf.newSAXParser();
	    	
	        xr = sp.getXMLReader();
	        
	        xr.setContentHandler(handler);
	        InputSource is = new InputSource(new StringReader(content)); 
	        xr.parse(is);

	        result = handler.getMessages();
	    } catch (SAXException e) {
	        //e.printStackTrace();
	    } catch (IOException e) {
	        //e.printStackTrace();
	    } catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	    return result;
	}
	
	public String parseAverage(String content) {

		AverageHandler handler = new AverageHandler();
		String result = new String();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}

	public LinkedList<StudentData> parseStudentsList(String content)
	{
		StudentHandler handler = new StudentHandler();
		LinkedList<StudentData> result = new LinkedList<StudentData>();

		XMLReader xr;
		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();

		} catch (Exception e) {
			// e.printStackTrace();
		}

		return result;
	}

	public List<TeacherScheduleData> parseTeacherScheduleData(String content) {

		TeacherScheduleHandler handler = new TeacherScheduleHandler();
		List<TeacherScheduleData> result = new ArrayList<TeacherScheduleData>();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();
	
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}

	public List<TeacherLessonData> parseTeacherLessonsData(String content) {

		TeacherLessonHandler handler = new TeacherLessonHandler();
		List<TeacherLessonData> result = new ArrayList<TeacherLessonData>();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();

		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
	
	public List<TeacherExamData> parseTeacherExamsData(String content) {

		TeacherExamsHandler handler = new TeacherExamsHandler();
		List<TeacherExamData> result = new ArrayList<TeacherExamData>();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();
	
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
	
	public List<RequestData> parseRequestsData(String content) {

		RequestsHandler handler = new RequestsHandler();
		List<RequestData> result = new ArrayList<RequestData>();
		XMLReader xr;

		SAXParserFactory spf = SAXParserFactory.newInstance();

		try {
			SAXParser sp = spf.newSAXParser();

			xr = sp.getXMLReader();

			xr.setContentHandler(handler);
			InputSource is = new InputSource(new StringReader(content));
			xr.parse(is);

			result = handler.getMessages();
	
		} catch (SAXException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			// e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}

}
