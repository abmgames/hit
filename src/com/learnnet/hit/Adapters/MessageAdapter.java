package com.learnnet.hit.Adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.learnnet.hit.R;
import com.learnnet.hit.Objects.MessageData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MessageAdapter extends ArrayAdapter<MessageData> {

	public Activity activity;
	public List<MessageData> tweets;

	public MessageAdapter(Activity a, int textViewResourceId, List<MessageData> tweets) {
		super(a, textViewResourceId, tweets);
		this.tweets = tweets;
		activity = a;
	}

	public static class ViewHolder {
		public TextView text1;
		public TextView text2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		ViewHolder holder;

		final MessageData tweet = tweets.get(position);

		if (v == null) {

			LayoutInflater vi = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.messages_list_item, null);
			holder = new ViewHolder();
			holder.text1 = (TextView) v.findViewById(R.id.line1);
			holder.text2 = (TextView) v.findViewById(R.id.line2);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		if (tweet != null) {
			holder.text1.setText(tweet.getDTUPD().substring(0, tweet.getDTUPD().indexOf(" ")));
			holder.text2.setText(tweet.getTITLE());
		}

		return v;

	}

}
