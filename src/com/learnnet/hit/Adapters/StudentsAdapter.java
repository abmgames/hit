package com.learnnet.hit.Adapters;


import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.learnnet.hit.Objects.StudentData;
import com.learnnet.hit.R;

import java.util.LinkedList;

public class StudentsAdapter extends BaseAdapter {

    private Activity context;
    private LinkedList<StudentData> studentsList;

    //Dimensions
    private int rowHeight, rowWidth, vIconSize;
    private float studentInfoTextHeight, studentNameTextSize;

    public StudentsAdapter(Activity context, LinkedList<StudentData> studentsList){
        this.context = context;
        this.studentsList = studentsList;

        //Set row dimensions
        try{
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            rowHeight = (int)(metrics.heightPixels * 0.09);
            rowWidth = metrics.widthPixels;

            studentInfoTextHeight = pixelsToSp(rowHeight * 0.5f * 0.37f);
            studentNameTextSize = pixelsToSp(rowHeight * 0.5f * 0.57f);
            vIconSize = (int)(rowHeight * 0.5f);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return (studentsList == null) ? 0 :studentsList.size();
    }

    @Override
    public StudentData getItem(int position) {
        return (studentsList == null) ? null : studentsList.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    //Add these methods to prevent reuse of rows...
    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    //Add these methods to prevent reuse of rows...(UP)

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if(convertView == null)
        {
            //First build
            LayoutInflater inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            //Initialize
            convertView = inflator.inflate(R.layout.presence_list_item, null);
            holder = new ViewHolder();

            //Set holder elements
            holder.container = (LinearLayout) convertView.findViewById(R.id.container);
            holder.studentImage = (ImageView) convertView.findViewById(R.id.student_image);
            holder.studentName = (TextView) convertView.findViewById(R.id.student_name);
            holder.studentCourse = (TextView) convertView.findViewById(R.id.student_course);
            holder.vIcon = (ImageView) convertView.findViewById(R.id.v_icon);
            holder.buttonsContainer = (LinearLayout) convertView.findViewById(R.id.buttons_container);
            holder.absenceButton = (TextView) convertView.findViewById(R.id.absence_button);
            holder.lateButton = (TextView) convertView.findViewById(R.id.late_button);

            //Get student details
            final StudentData studentDetails = getItem(position);

            //Update holder elements
            if(studentDetails.IMAGE != null)
                holder.studentImage.setImageBitmap(studentDetails.IMAGE);

            holder.studentName.setText(studentDetails.STUDENTNAME);
            holder.studentCourse.setText(studentDetails.STUDENTINFO);

            //Update V icon params
            FrameLayout.LayoutParams vIconParams = new FrameLayout.LayoutParams(vIconSize, vIconSize);

            vIconParams.gravity = Gravity.CENTER | Gravity.RIGHT;
            vIconParams.setMargins(0, 0, (int)(500 * 0.04), 0);

            holder.vIcon.setLayoutParams(vIconParams);

            convertView.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, rowHeight));
            convertView.setTag(holder);
        }
        /*else
            holder = (ViewHolder) convertView.getTag();*/


        return convertView;
    }

    public float pixelsToSp(float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }

    public class ViewHolder{

        public LinearLayout container;
        public ImageView studentImage;
        public TextView studentName;
        public TextView studentCourse;
        public ImageView vIcon;
        public LinearLayout buttonsContainer;
        public TextView absenceButton;
        public TextView lateButton;

    }
}
