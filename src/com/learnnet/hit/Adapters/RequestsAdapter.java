package com.learnnet.hit.Adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.learnnet.hit.R;
import com.learnnet.hit.Objects.RequestData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class RequestsAdapter extends ArrayAdapter<RequestData> {

	public Activity activity;
	public List<RequestData> tweets;

	public RequestsAdapter(Activity a, int textViewResourceId, List<RequestData> tweets) {
		super(a, textViewResourceId, tweets);
		this.tweets = tweets;
		activity = a;
	}

	public static class ViewHolder {
		public TextView text1;
		public TextView text2;
		public TextView text3;
		public TextView text4;
		public TextView text5;
		public TextView text6;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		ViewHolder holder;

		final RequestData tweet = tweets.get(position);

		if (v == null) {

			LayoutInflater vi = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.request_list_item, null);
			holder = new ViewHolder();
			holder.text1 = (TextView) v.findViewById(R.id.line1);
			holder.text2 = (TextView) v.findViewById(R.id.line2);
			holder.text3 = (TextView) v.findViewById(R.id.line3);
			holder.text4 = (TextView) v.findViewById(R.id.line4);
			holder.text5 = (TextView) v.findViewById(R.id.line5);
			holder.text6 = (TextView) v.findViewById(R.id.line6);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		if (tweet != null) {
			holder.text1.setText(tweet.topic);
			holder.text2.setText(tweet.subject);
			holder.text3.setText(tweet.date);
			holder.text4.setText(tweet.body);
			holder.text5.setText(tweet.status);
			holder.text6.setText(tweet.replyBody);
		}

		return v;
	}
}
