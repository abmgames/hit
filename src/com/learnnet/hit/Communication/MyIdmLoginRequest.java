package com.learnnet.hit.Communication;

import java.io.DataInputStream;
import java.io.IOException;
import java.security.KeyStore;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import com.learnnet.hit.Objects.IdmLoginData;
import com.learnnet.hit.Utils.xmlParser;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MyIdmLoginRequest {
	
	public IdmLoginData login(String idmUrl, String michlolUrl, String username, String password) {
		
		IdmLoginData result = null;
		
		try {
		HttpClient httpClient = createHttpClient();
		
		HttpPost httppost = new HttpPost(idmUrl);          
		httppost.setHeader("SOAPAction", michlolUrl);
		httppost.setHeader("Content-Type", "text/xml; charset=utf-8");  
	
		HttpEntity entity = new StringEntity(soapRequest(username, password),
				HTTP.UTF_8);
		httppost.setEntity(entity);
		
		HttpResponse httpResponse = httpClient.execute(httppost);
		HttpEntity r_entity = httpResponse.getEntity();

		xmlParser xp = new xmlParser();
		result = xp.parseOutputDataNewLogin(new DataInputStream(
				r_entity.getContent()));

		}
		catch (IOException e) {
			
		}
		
		return result;
	}
	
	private HttpClient createHttpClient() {
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

			return new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}
	
	private String soapRequest(String user, String password) {
		final StringBuffer soap = new StringBuffer(); // Comment: 4
		soap.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		soap.append("<soapenv:Envelope xmlns:ws=\"http://ws.edp\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");
		soap.append("<soapenv:Header/>\n");
		soap.append("<soapenv:Body>\n");
		soap.append("<ws:authRequest>\n");
		soap.append("<uid>" + user + "</uid>\n");
		soap.append("<password>" + password + "</password>\n");
		soap.append("</ws:authRequest>\n");
		soap.append("</soapenv:Body>\n");
		soap.append("</soapenv:Envelope>\n");

		return soap.toString();

	}
	
}
