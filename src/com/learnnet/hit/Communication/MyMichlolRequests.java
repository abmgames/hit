package com.learnnet.hit.Communication;

import android.text.TextUtils;
import android.util.Log;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import com.learnnet.hit.HebrewCalendar.JewishCalendar;
import com.learnnet.hit.MyApplication;
import com.learnnet.hit.Utils.xmlParser;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class MyMichlolRequests {

	private static final String RASHIM_DEMO_API_URL = "http://rashim.co.il/wsm3api/michlolapi.asmx";
	private static final List<Integer> ATTACH_DATA_REQUESTS = new ArrayList<Integer>(Arrays.asList(new Integer[]{
			4,  //Grades
			19, //Messages
			27, //SCHEDULE
			28, //Exams
			32, //Student requests
	}));

	public class MySSLSocketFactory extends SSLSocketFactory {
		SSLContext sslContext = SSLContext.getInstance("TLS");

		public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
			super(truststore);

			TrustManager tm = new X509TrustManager() {
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};

			sslContext.init(null, new TrustManager[] { tm }, null);
		}

		@Override
		public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
			return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
		}

		@Override
		public Socket createSocket() throws IOException {
			return sslContext.getSocketFactory().createSocket();
		}
	}

	public HttpClient getNewHttpClient() {
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			// set the connection timeout value to 10 seconds
		    HttpConnectionParams.setConnectionTimeout(params, 10000);
		    
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

			return new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}

	public String call(String reqID, String user, String password,
					   String option1, String option2, String kMichlolApiURL, boolean isUsingIdm){

		return call(reqID, user, password, option1, option2, kMichlolApiURL, isUsingIdm, 0, "");
	}

	public String call(String reqID, String user, String password,
					   String option1, String option2, String kMichlolApiURL, boolean isUsingIdm, int meetingId, String admissions) {

		if(!MyApplication.pKeyIsAvailable())
			return "";

		StringBuilder result = new StringBuilder();

		if (!checkValidation(reqID) || !checkValidation(user)
				|| !checkValidation(password)) {
			return result.toString();
		}


		HttpClient httpclient = getNewHttpClient();

		HttpPost httppost = new HttpPost(MyApplication.DEBUG_MODE ? RASHIM_DEMO_API_URL : kMichlolApiURL);

		httppost.setHeader("SOAPAction", "http://RashimApi.co.il/ProcessRequest");
		httppost.setHeader("Content-Type", "text/xml; charset=utf-8");

		try {

			// String temp =
			// soapRequest(body(Integer.valueOf(reqID),user,password,option1,option2));

			HttpEntity entity = new StringEntity(soapRequest(body(
					Integer.valueOf(reqID), user, password, option1, option2, isUsingIdm, meetingId, admissions),Integer.valueOf(reqID)),
					HTTP.UTF_8);
			httppost.setEntity(entity);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity r_entity = response.getEntity();

			if (r_entity.isStreaming() && r_entity != null) {

				// soapXMLParser soapParser = new soapXMLParser();
				// result = soapParser.parse(new
				// DataInputStream(r_entity.getContent()));
				xmlParser xp = new xmlParser();
				result.append(xp.parseOutputData(new DataInputStream(r_entity
						.getContent())));

//				Log.e("result:", result.toString());
			}
			httpclient.getConnectionManager().shutdown();

		} catch (Exception E) {
			return null;
		}

		return result.toString();
	}

	private String soapRequest(String body , int reqID) {
		final StringBuffer soap = new StringBuffer(); // Comment: 4
		soap.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		soap.append("<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n");
		soap.append(	"<SOAP:Body>\n");
		soap.append(		"<ProcessRequest xmlns=\"http://RashimApi.co.il\">\n");
		soap.append(			"<P_RequestParams>");
		soap.append(				body);
		soap.append(			"</P_RequestParams>");
		soap.append(			"<Authenticator>");
		soap.append(				"<UserName>" + MyApplication.Kxx[0] + "</UserName>");
		soap.append(				"<Password>" + MyApplication.Kxx[1] + "</Password>");
		soap.append(			"</Authenticator>");
		soap.append(		"</ProcessRequest>\n");
		soap.append(	"</SOAP:Body>\n");
		soap.append("</SOAP:Envelope>");

		String soapRequest = soap.toString();

		if(MyApplication.DEBUG_MODE)
			Log.i("DEBUG", soapRequest);

		return soapRequest;

	}

	private String body(int reqID, String user, String password,
						String option1, String option2, boolean isUsingIdm, int meetingId, String admissions) {

		// user = String.valueOf(Integer.valueOf(user));

		StringBuffer body = new StringBuffer();

		body.append("<RequestID>" + String.valueOf(reqID) + "</RequestID>");
		body.append("<InputData>");

		body.append("&lt;?xml version=\"1.0\" encoding=\"utf-8\" ?&gt;\n");
		body.append("&lt;PARAMS&gt;\n");

		if(reqID != 48 && !TextUtils.isEmpty(MyApplication.TOKEN))
		{
			body.append("&lt;TOKEN&gt;" + MyApplication.TOKEN + "&lt;/TOKEN&gt;\n");

			if(ATTACH_DATA_REQUESTS.contains(reqID)){
				body.append("&lt;USERNAME&gt;" + MyApplication.pU + "&lt;/USERNAME&gt;\n");
				body.append("&lt;PASSWORD&gt;" + MyApplication.pP + "&lt;/PASSWORD&gt;\n");
			}
		}

		switch (reqID) {
			case 48: //Login
				if (isUsingIdm) {
					//Only for achva
					body.append("&lt;ZHT&gt;" + user + "&lt;/ZHT&gt;\n");
					body.append("&lt;PASSWORD&gt;&lt;/PASSWORD&gt;\n");
					body.append("&lt;SNL&gt;" + getHebYear() + "&lt;/SNL&gt;\n");
					body.append("&lt;USERNAME&gt;" + password + "&lt;/USERNAME&gt;\n");
					body.append("&lt;RETHASIMA&gt;1&lt;/RETHASIMA&gt;\n");
				}
				else {
					body.append("&lt;ZHT&gt;" + user + "&lt;/ZHT&gt;\n");
					body.append("&lt;USERNAME&gt;&lt;/USERNAME&gt;\n");

					//Only for achva!
					//body.append("&lt;USERNAME&gt;" + user + "&lt;/USERNAME&gt;\n");
					//body.append("&lt;ZHT&gt;&lt;/ZHT&gt;\n");

					body.append("&lt;PASSWORD&gt;" + password + "&lt;/PASSWORD&gt;\n");
					body.append("&lt;SNL&gt;" + getHebYear() + "&lt;/SNL&gt;\n");

					body.append("&lt;RETHASIMA&gt;1&lt;/RETHASIMA&gt;\n");
				}
				break;
			case 4: //Grades
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				body.append("&lt;YEAR&gt;" + option1 + "&lt;/YEAR&gt;\n");
				break;
			case 19: //Messages
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				body.append("&lt;MSG_TYPE&gt;0&lt;/MSG_TYPE&gt;\n");
				break;
			case 20:
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				body.append("&lt;MARKS_COUNT&gt;" + 50 + "&lt;/MARKS_COUNT&gt;\n");
				break;
			case 23: //TEACHER SCHEDULE
				body.append("&lt;ZHT&gt;" + user + "&lt;/ZHT&gt;\n");
				body.append("&lt;SNL&gt;" + getHebYear() + "&lt;/SNL&gt;\n");
				break;
			case 27: //SCHEDULE
				body.append("&lt;STUDENTID&gt;" + user + "&lt;/STUDENTID&gt;\n");
				body.append("&lt;BEGINDATE&gt;" + option1 + "&lt;/BEGINDATE&gt;\n");
				body.append("&lt;ENDDATE&gt;" + option2 + "&lt;/ENDDATE&gt;\n");
				break;
			case 28: //Exams
				body.append("&lt;STUDENTID&gt;" + user + "&lt;/STUDENTID&gt;\n");
				body.append("&lt;YEAR&gt;" + getHebYear() + "&lt;/YEAR&gt;\n");
				body.append("&lt;CATEGORY&gt;" + option1 + "&lt;/CATEGORY&gt;\n");
				break;
			case 32: //Student requests
				body.append("&lt;STUDENTID&gt;" + user + "&lt;/STUDENTID&gt;\n");
				break;
			case 36:
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				body.append("&lt;COURSE_ID&gt;" + option1 + "&lt;/COURSE_ID&gt;\n");
				break;
			case 37:
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				break;
			case 39:
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				body.append("&lt;Year&gt;" + getHebYear() + "&lt;/Year&gt;\n");
				break;
			case 49:
				body.append("&lt;TEACHER_ID&gt;" + user + "&lt;/TEACHER_ID&gt;\n");
				body.append("&lt;YEAR&gt;" + getHebYear() + "&lt;/YEAR&gt;\n");
				break;
			case 52:
				body.append("&lt;TEACHERID&gt;" + user + "&lt;/TEACHERID&gt;\n");
				body.append("&lt;DATE&gt;" + option1 + "&lt;/DATE&gt;\n");
				body.append("&lt;TODATE&gt;" + option2 + "&lt;/TODATE&gt;\n");

				break;
			case 63:
				body.append("&lt;MEETINGID&gt;" + meetingId + "&lt;/MEETINGID&gt;\n");
				body.append("&lt;INCLUDEIMAGE&gt;1&lt;/INCLUDEIMAGE&gt;\n");
				body.append("&lt;ZHT&gt;" + user + "&lt;/ZHT&gt;\n");

				break;

			case 64:
				body.append("&lt;MEETING_ID&gt;" + meetingId + "&lt;/MEETING_ID&gt;\n");
				body.append("&lt;ADMISIONS&gt;" + admissions + "&lt;/ADMISIONS&gt;\n");
				body.append("&lt;ZHT&gt;" + user + "&lt;/ZHT&gt;\n");

				break;
			default:
				break;
		}

		body.append("&lt;/PARAMS&gt;\n");
		body.append("</InputData>");

		return body.toString();
	}

	private Boolean checkValidation(String testString) {
		if (testString.contains("<") || testString.contains(">")
				|| testString.contains("\"") || testString.contains("\'")
				|| (testString == "")) {
			return false;
		} else {
			return true;
		}

	}

	public String getHebYear() {

		JewishCalendar jc = new JewishCalendar(new Date());

		int year = jc.getJewishYear();

		Map<String, String> map = new HashMap<String, String>();

		map.put("1", "א");
		map.put("2", "ב");
		map.put("3", "ג");
		map.put("4", "ד");
		map.put("5", "ה");
		map.put("6", "ו");
		map.put("7", "ז");
		map.put("8", "ח");
		map.put("9", "ט");
		map.put("10", "י");
		map.put("20", "כ");
		map.put("30", "ל");
		map.put("40", "מ");
		map.put("50", "נ");
		map.put("60", "ס");
		map.put("70", "ע");
		map.put("80", "פ");
		map.put("90", "צ");
		map.put("100", "ק");
		map.put("200", "ר");
		map.put("300", "ש");
		map.put("400", "ת");

		year = year % 1000;

		StringBuilder yearString = new StringBuilder();

		while (year > 0) {
			// Get Max Value
			int currentValue = 400;
			while ((currentValue > year) && (currentValue > 100)) {
				currentValue -= 100;
			}
			while ((currentValue > year) && (currentValue > 10)) {
				currentValue -= 10;
			}
			while ((currentValue > year) && (currentValue > 0)) {
				currentValue -= 1;
			}
			yearString.append(map.get(String.valueOf(currentValue)));
			year -= currentValue;
		}

		return yearString.toString();

	}

}
