package com.learnnet.hit.activity;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.learnnet.hit.MyApplication;
import com.learnnet.hit.R;
import com.learnnet.hit.Communication.MyMichlolRequests;
import com.learnnet.hit.Fragments.ExamExpandFragment;
import com.learnnet.hit.Fragments.ExamsFragment;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Objects.ExamData;
import com.learnnet.hit.Utils.GlobalDefs;
import com.learnnet.hit.Utils.xmlParser;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class Exams extends ActionBarActivity implements GlobalDefs, MyInterface, OnItemSelectedListener {

	// =================================================
	// FIELDS
	// =================================================

	private MyApplication mApp;
	private int mCurrentFragmentIndex;
	private ActionBar mActionBar;
	private ProgressDialog spinnerDialog;
	private String examTitle;
	private String examDate;
	private String examTime;
	private String examLecturer;
	private String examClassRoom;
	private List<ExamData> mDataFutureExams;
	private List<ExamData> mDataPastExams;
	private TextView actionBarTitle;
	private int option1;
	private int spinnerIndex;

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_exams);

		this.mApp = (MyApplication)getApplication();

//		if (mApp.mCurrentUserDetails == null) {
//			MyApplication.isAppKilled = true;
//			Intent i = new Intent(Exams.this, Splash.class);
//			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			startActivity(i);
//			finish();
//		}
//		else {

			// Inflate your custom action bar layout
			final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
					R.layout.action_bar_centered_text, null);

			// Set up your ActionBar
			mActionBar = getSupportActionBar();
			mActionBar.setDisplayShowCustomEnabled(true);
			mActionBar.setCustomView(actionBarLayout);

			// Disable drawer swipe gesture 
			DrawerLayout mDrawerLayout;
			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED); 

			// Enable ActionBar app icon to behave as action to toggle nav drawer
			mActionBar.setDisplayShowHomeEnabled(false);
			mActionBar.setDisplayUseLogoEnabled(false);
			mActionBar.setDisplayHomeAsUpEnabled(false);
			mActionBar.setDisplayShowTitleEnabled(false);

			// Set ActionBar's text
			actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
			actionBarTitle.setText(getResources().getString(R.string.my_exams_title));

			if (savedInstanceState == null) {
				option1 = 1;
				new GetExamsTask().execute(option1);
			}
//		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_inner, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			return (String) mApp.mPropertiesSpecific.get(key);
		}
		else {
			return null;
		}
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBackPressed() {
		switch (mCurrentFragmentIndex) {
		case 0:
			super.onBackPressed();
			break;
		case 1:
			selectItem(0);
			break;
		default:
			super.onBackPressed(); // Open default fragment (Messages menu)
			break;
		}
	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================

	// =================================================
	// CLASS LOGIC
	// =================================================

	private void selectItem(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new ExamsFragment();
			actionBarTitle.setText(getResources().getString(R.string.my_exams_title));
			break;
		case 1:
			fragment = new ExamExpandFragment();
			Bundle args = new Bundle();
			args.putString(BUNDLE_EXAM_TITLE, examTitle);
			args.putString(BUNDLE_EXAM_DATE, examDate);
			args.putString(BUNDLE_EXAM_TIME, examTime);
			args.putString(BUNDLE_EXAM_LECTURER, examLecturer);
			args.putString(BUNDLE_EXAM_CLASS, examClassRoom);
			fragment.setArguments(args);
			break;
		default:
			fragment = new ExamsFragment();
			break;
		}
		if (fragment != null) {

			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			mCurrentFragmentIndex = position;
		}
	}

	public void displaySpinner() {
		spinnerDialog = ProgressDialog.show(Exams.this, "",
				getString(R.string.my_wait), true);
	}

	public void hideSpinner() {
		spinnerDialog.cancel();
	}

	public void openExam(ExamData msg) {
		actionBarTitle.setText(msg.COURSE_DESCRIPTION.replaceAll("\\d+", "").replace("-", ""));
		examTitle = msg.COURSE_DESCRIPTION;
		examDate = msg.EXAM_DATE_INFO;
		examTime = msg.EXAM_TIME_INFO;
		examLecturer = msg.TEACHER_NAME;
		examClassRoom = msg.ROOM_NAME;
		selectItem(1);
	}

	private String michlolRequestExams(String username, String url, int option1) {
		MyMichlolRequests michlolExamsRequest = new MyMichlolRequests();
		return michlolExamsRequest.call(REQ_EXAMS, username, "UnsedValueJustForChecks",
				Integer.toString(option1), null, url, false); // false doesn't matter here.
	}

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	public MyApplication getMyApp() {
		return this.mApp;
	}

	public List<ExamData> getExamsData(int examType) {
		switch (examType) {
		case 0:
			return this.mDataFutureExams;
		case 1:
			return this.mDataPastExams;
		default:
			return this.mDataFutureExams;
		}
	}

	public void setSpinnerIndex(int index) {
		this.spinnerIndex = index;
	}

	public int getSpinnerIndex() {
		return this.spinnerIndex;
	}

	// =================================================
	// INNER CLASSES
	// =================================================

	private class GetExamsTask extends AsyncTask<Integer, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			displaySpinner();
		}

		@Override
		protected Void doInBackground(Integer... arg0) {

			String username = mApp.mPrefs.getString(PREF_USER_NAME, "");
			String url = mApp.mPrefs.getString(PREF_MICHLOL_URL, "");


			// Getting first half of the exams

			StringBuilder sb = new StringBuilder();
			sb.append(michlolRequestExams(username, url, arg0[0])); // Getting future exams

			xmlParser xp = new xmlParser();
			mDataFutureExams = xp.parseExamData(sb.toString());

			Collections.sort(mDataFutureExams);

			// Getting second half of the exams

			StringBuilder sb2 = new StringBuilder();
			sb2.append(michlolRequestExams(username, url, arg0[0] + 1)); // Getting past exams
			xp = new xmlParser();
			mDataPastExams = xp.parseExamData(sb2.toString());
			Collections.sort(mDataPastExams);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			hideSpinner();
			selectItem(0);
		}
	}
}