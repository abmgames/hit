package com.learnnet.hit.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.learnnet.hit.MyApplication;
import com.learnnet.hit.R;
import com.learnnet.hit.Adapters.MyDrawerListViewAdapter;
import com.learnnet.hit.Communication.MyMichlolRequests;
import com.learnnet.hit.Fragments.AboutFragment;
import com.learnnet.hit.Fragments.AcademicMenuFragment;
import com.learnnet.hit.Fragments.AcademicTeacherMenuFragment;
import com.learnnet.hit.Fragments.FacebookFragment;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Utils.GlobalDefs;
import com.learnnet.hit.Utils.xmlParser;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MainMenu extends ActionBarActivity implements GlobalDefs, MyInterface {

	// =================================================
	// FIELDS
	// =================================================
	
	private static boolean isTeacher;
	private MyApplication mApp;
	private Fragment mCurrentFragment;
	private boolean mHasSelectionChanged;
	private int mCurrentFragmentIndex = -1;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerListView;
	private ActionBarDrawerToggle mDrawerToggle;
	private ActionBar mActionBar;
	private ProgressDialog spinnerDialog;
	private final String[] mDrawerTitles = {"תפריט אקדמי", "Dropbox", "Facebook", "Youtube", "אגודת הסטודנטים", "דואר מערכת", "אודות", "יציאה"}; 
	private final String[] mDrawerImages = {"academic_menu_icon", "dropbox_icon", "facebook_icon", "youtube_icon", "aguda_icon", "inbox_icon", "about_icon", "exit_icon"};
	private AlertDialog alertDialog;

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.mApp = (MyApplication)getApplication();
		this.mHasSelectionChanged = true;

		// Inflate your custom layout
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar,
				null);

		// Set up your ActionBar
		mActionBar = getSupportActionBar();
		mActionBar.setIcon(R.drawable.drawer_menu_icon);
			    
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setCustomView(actionBarLayout);

//		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerListView = (ListView) findViewById(R.id.left_drawer);

		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		// set up the drawer's list view with items and click listener
		ArrayList<Map<String, String>> data = new ArrayList<Map<String,String>>();
		for (int i=0 ; i<mDrawerTitles.length ; i++) {
			Map<String, String> map = new HashMap<String, String>();
			map.put(DRAWER_TITLE, mDrawerTitles[i]);
			map.put(DRAWER_IMAGE, mDrawerImages[i]);
			data.add(map);
		}
		mDrawerListView.setAdapter(new MyDrawerListViewAdapter(this, data));
		mDrawerListView.setOnItemClickListener(new DrawerItemClickListener());

		// enable ActionBar app icon to behave as action to toggle nav drawer
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setDisplayShowTitleEnabled(false);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setCustomView(R.layout.action_bar);
		mActionBar.setDisplayShowHomeEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(
				this,                  /* host Activity */
				mDrawerLayout,         /* DrawerLayout object */
				R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
				android.R.drawable.ic_dialog_alert,  /* "open drawer" description for accessibility */
				android.R.drawable.ic_delete  /* "close drawer" description for accessibility */
				) {
			
			public void onDrawerClosed(View view) {
				if (mHasSelectionChanged) {
					if (mCurrentFragment != null) {
						FragmentManager fragmentManager = getSupportFragmentManager();
						fragmentManager.beginTransaction().replace(R.id.content_frame, mCurrentFragment).commit();
						mHasSelectionChanged = false;
					}
				}
			}

			public void onDrawerOpened(View drawerView) {

			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		TextView actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
		actionBarTitle.setText("");
		
		if (savedInstanceState == null) {
			if (Integer.parseInt(mApp.mCurrentUserDetails.TEACHER_ID) > 0) {
				choosePersonality();
			}
			else {
				selectItem(0);
				FragmentManager fragmentManager = getSupportFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.content_frame, mCurrentFragment).commit();
				mHasSelectionChanged = false;
			}
		}

		getStudentYears();
		
		displayWelcomeMessage();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content view
		//		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		//		menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action buttons
		switch(item.getItemId()) {
		case R.id.ivActionBarSupport:
			sendSupportMail();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void setTitle(CharSequence title) {

	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			return (String) mApp.mPropertiesSpecific.get(key);
		}
		else {
			return null;
		}
	}

	@Override
	public int getPropertiesInt(String key) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onBackPressed() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		switch (mCurrentFragmentIndex) {
		case 0:
			super.onBackPressed();
			break;
		case 2:
			if (mCurrentFragment != null) {
				WebView wv = ((FacebookFragment)mCurrentFragment).getWebView();
				if (wv != null) {
					if (wv.canGoBack()) {
						wv.goBack();
					}
					else {
						selectItem(0);
					}
				}
				fragmentManager.beginTransaction().replace(R.id.content_frame, mCurrentFragment).commit();
				mHasSelectionChanged = false;
			}
			else {
				super.onBackPressed();
			}
			break;
		case 6:
			if (mCurrentFragment != null) {
				WebView wvAbout = ((AboutFragment)mCurrentFragment).getWebView();
				if (wvAbout != null) {
					if (wvAbout.canGoBack()) {
						wvAbout.goBack();
					}
					else {
						selectItem(0);
					}
				}
				fragmentManager.beginTransaction().replace(R.id.content_frame, mCurrentFragment).commit();
				mHasSelectionChanged = false;
			}
			else {
				super.onBackPressed();
			}
			break;
		default:
			super.onBackPressed(); // Open default fragment (Academic menu)
			break;
		}
	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================

	// =================================================
	// CLASS LOGIC
	// =================================================

	private void selectItem(int position) {
		// update the main content by replacing fragments
		if (mCurrentFragmentIndex != position) {
			Fragment fragment = null;
			switch (position) {
			case 0:
				// Academic menu
				if (isTeacher) {
					fragment = new AcademicTeacherMenuFragment();
				}
				else {
					fragment = new AcademicMenuFragment();
				}
				changeFragment(fragment, position);
				break;
			case 1:
				// Dropbox
				mApp.sendAnalyticScreen(ANALYTIC_DROP_BOX);
				PackageManager manager = getPackageManager();
				Intent dbIntent = manager.getLaunchIntentForPackage("com.dropbox.android");
				if (dbIntent != null) {
					dbIntent.addCategory(Intent.CATEGORY_LAUNCHER);
					startActivity(dbIntent);
				}
				else {
					displayDropBoxNotInstalledMsg();
				}
				mDrawerLayout.closeDrawer(mDrawerListView);
				break;
			case 2:
				// Facebook
				mApp.sendAnalyticScreen(ANALYTIC_FACE_BOOK);
				if (MyApplication.isInternetAvailable) {
					String facebookUrl = getPropertiesString(PROP_FACEBOOK_URL);
					if (facebookUrl != null) {
						fragment = new FacebookFragment();
						Bundle args = new Bundle();
						args.putString(BUNDLE_URL, facebookUrl);
						fragment.setArguments(args);
					}
					else {
						Toast.makeText(this, R.string.menu_no_link, Toast.LENGTH_SHORT).show();
					}
					changeFragment(fragment, position);
				}
				else {
					mApp.showNoInternetDialog(this);
					mDrawerLayout.closeDrawer(mDrawerListView);
				}
				break;
			case 3:
				// Youtube
				mApp.sendAnalyticScreen(ANALYTIC_YOU_TUBE);
				if (MyApplication.isInternetAvailable) {
					String youtubeUrl = getPropertiesString(PROP_YOUTUBE_URL);
					Intent i = new Intent(Intent.ACTION_VIEW);
				    i.setData(Uri.parse(youtubeUrl));
				    Intent chooser = Intent.createChooser(i, getResources().getString(R.string.my_app_chooser));
					startActivity(chooser);
					mDrawerLayout.closeDrawer(mDrawerListView);
				}
				else {
					mApp.showNoInternetDialog(this);
					mDrawerLayout.closeDrawer(mDrawerListView);
				}
				break;
			case 4:
				// Students Association
				if (MyApplication.isInternetAvailable) {
					mDrawerLayout.closeDrawer(mDrawerListView);
					String associationUrl = getPropertiesString(PROP_STUD_ASSOC);
					Intent i = new Intent(Intent.ACTION_VIEW);
				    i.setData(Uri.parse(associationUrl));
				    Intent chooser = Intent.createChooser(i, getResources().getString(R.string.my_app_chooser));
					startActivity(chooser);
					
				}
				else {
					mApp.showNoInternetDialog(this);
					mDrawerLayout.closeDrawer(mDrawerListView);
				}
				break;
			case 5:
				// Inbox
				if (MyApplication.isInternetAvailable) {
					mDrawerLayout.closeDrawer(mDrawerListView);
					Intent inboxIntent = new Intent(this, Inbox.class);
					startActivity(inboxIntent);
				}
				else {
					mApp.showNoInternetDialog(this);
					mDrawerLayout.closeDrawer(mDrawerListView);
				}
				break;
			case 6:
				// About
				if (MyApplication.isInternetAvailable) {
					String aboutUrl = getPropertiesString(PROP_ABOUT);
					if (aboutUrl != null) {
						fragment = new AboutFragment();
						Bundle args = new Bundle();
						args.putString(BUNDLE_URL, aboutUrl);
						fragment.setArguments(args);
					}
					else {
						Toast.makeText(this, R.string.menu_no_link, Toast.LENGTH_SHORT).show();
					}
					changeFragment(fragment, position);
				}
				else {
					mApp.showNoInternetDialog(this);
					mDrawerLayout.closeDrawer(mDrawerListView);
				}
				break;
			case 7:
				// Logout
				Intent exitIntent = new Intent(this, Login.class);
				startActivity(exitIntent);
				this.finish();
				break;
			default:
				// Do nothing
				break;
			}
			if (fragment != null) {

			}
			else {

			}
		}
		else {
			mDrawerLayout.closeDrawer(mDrawerListView);
		}
	}
	
	private void changeFragment(Fragment fragment, int position) {
		// TODO : [A+] Finish this method and remove fragment transition from selectItem method.
		if (mCurrentFragmentIndex != position) {
			mCurrentFragmentIndex = position;
			mHasSelectionChanged = true;
		}
		
		TextView actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
		actionBarTitle.setText(mDrawerTitles[position]);

		mCurrentFragment = fragment;

		// update selected item and title, then close the drawer
		mDrawerListView.setItemChecked(position, true);
		//		setTitle(mDrawerTitles[position]);
		mDrawerLayout.closeDrawer(mDrawerListView);
	}
	
	private void sendSupportMail() {
		
		
		try {
			PackageManager manager = this.getPackageManager();
			PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
			
			final Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + getPropertiesString(PROP_SUPPORT_MAIL)));
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, MAIL_SUBJECT);
			emailIntent.putExtra(Intent.EXTRA_TEXT, MAIL_BODY + " " + getPropertiesString(PROP_BRANCH_ID)
					+ " , Android API. " + android.os.Build.VERSION.SDK_INT
					+ " , Android ver. " + android.os.Build.VERSION.RELEASE + MAIL_DO_NOT_DELETE + " \r\nApp code version: " + info.versionCode);
			startActivity(Intent.createChooser(emailIntent, "Send email..."));
			
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void getStudentYears() {
		final String username = mApp.mPrefs.getString(PREF_USER_NAME, "");
		final String url = mApp.mPrefs.getString(PREF_MICHLOL_URL, "");
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				MyMichlolRequests myMichlolYearsRequest = new MyMichlolRequests();
				
				String result = myMichlolYearsRequest.call(REQ_YEARS, username, "UnsedValueJustForChecks",
						null, null, url, false);
				
				if (result != null) {
					parseMichlolResponse(result);
				}
			}
		});
		t.start();
	}
	
	private void parseMichlolResponse(String response) {
		mApp.mCurrentUserYears = null;
		xmlParser xp = new xmlParser();
		mApp.mCurrentUserYears = xp.parseYearsData(response);
	}
	
	public void displaySpinner() {
		spinnerDialog = ProgressDialog.show(MainMenu.this, "",
				getString(R.string.my_wait), true);
	}
	
	public void hideSpinner() {
		spinnerDialog.cancel();
	}
	
	private void displayDropBoxNotInstalledMsg() {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();

		alertDialog.setTitle(getString(R.string.my_error_general_title));

		alertDialog.setMessage(getString(R.string.my_error_no_drop_box));

		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
				getString(R.string.my_ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		try {
			alertDialog.show();
		} catch (Exception e) {
			// handle the exception
		}
	}
	
	public void displayAverageDialog(String avg) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();

		alertDialog.setTitle(getString(R.string.app_name));

		alertDialog.setMessage(getString(R.string.my_the_average) + " " + avg);

		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
				getString(R.string.my_ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		try {
			alertDialog.show();
		} catch (Exception e) {
			// handle the exception
		}
	}
	
	private void choosePersonality() {
		alertDialog = new AlertDialog.Builder(this).create();

		alertDialog.setTitle(getString(R.string.my_select_personality));

		alertDialog.setMessage(getString(R.string.my_select_personality_message));

		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
				getString(R.string.my_student),
				new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {
				isTeacher = false;
				dialog.cancel();
				selectItem(0);
				FragmentManager fragmentManager = getSupportFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.content_frame, mCurrentFragment).commit();
				mHasSelectionChanged = false;
			}
		});
		alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
				getString(R.string.my_teacher),
				new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {
				isTeacher = true;
				dialog.cancel();
				selectItem(0);
				FragmentManager fragmentManager = getSupportFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.content_frame, mCurrentFragment).commit();
				mHasSelectionChanged = false;
			}
		});
		alertDialog.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
                    
                }
                return true;
			}
		});

		try {
			alertDialog.show();
		} catch (Exception e) {
			// handle the exception
		}
	}
	
	private void displayWelcomeMessage() {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();

		alertDialog.setTitle(getString(R.string.my_welcome_msg_title));

		alertDialog.setMessage(getString(R.string.my_welcome_msg_body) + " " + mApp.mCurrentUserDetails.FULLNAME);

		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
				getString(R.string.my_ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		try {
			alertDialog.show();
		} catch (Exception e) {
			// handle the exception
		}
	}
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	public MyApplication getMyApp() {
		return mApp;
	}
	
	public static boolean getIsTeacher() {
		return isTeacher;
	}

	// =================================================
	// INNER CLASSES
	// =================================================
	
	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
		}
	}

}