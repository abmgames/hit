package com.learnnet.hit.activity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.learnnet.hit.MyApplication;
import com.learnnet.hit.R;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Utils.GlobalDefs;
import com.learnnet.hit.Utils.PlistReader;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class Splash extends Activity implements GlobalDefs, MyInterface {
	
	// =================================================
	// FIELDS
	// =================================================
	
	private MyApplication mApp;
	private TextView tvInternet;
	private ProgressBar pbSpinner;
	
	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_splash);
		this.mApp = (MyApplication)getApplication();
		
		if (isNetworkConnected()) {
			MyApplication.isInternetAvailable = true;
			DownloadFileAsync mDfa = new DownloadFileAsync();
			mDfa.execute(PLIST_PROPERTIES_FILE_LINK);
		}
		else {
			tvInternet = (TextView) findViewById(R.id.tvLoading);
			tvInternet.setText(R.string.splash_no_internet);
			pbSpinner = (ProgressBar) findViewById(R.id.pbSplashSpinner);
			pbSpinner.setVisibility(View.INVISIBLE);
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	@Override
	public boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			// There are no active networks.
			return false;
		}
		else {
			return true;
		}
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}
	
	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	
	
	// =================================================
	// CLASS LOGIC
	// =================================================

	private void startLoginActivity() {
		Intent i = new Intent(this, Login.class);
		startActivity(i);
		finish();
	}
	
	private boolean parsePList() {
		
		try {
			FileInputStream fis = new FileInputStream(getFilesDir() + "/" + PLIST_PROPERTOES_FILE_NAME);
			mApp.mProperties = PlistReader.ReadToMap(fis);
			if (mApp.mProperties.containsKey(INSTITUTE_NAME)) {
				mApp.mPropertiesSpecific = mApp.mProperties.get(INSTITUTE_NAME);
			}
			return true;
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			return false;
		} catch (XmlPullParserException e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
	}
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	// =================================================
	// INNER CLASSES
	// =================================================
	
	class DownloadFileAsync extends AsyncTask<String, String, String> {
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... aurl) {
			int count;

			try {

				URL url = new URL(aurl[0]);
				URLConnection conexion = url.openConnection();
				conexion.connect();

				int lenghtOfFile = conexion.getContentLength();
				
				InputStream input = new BufferedInputStream(url.openStream());
				OutputStream output = new FileOutputStream(getFilesDir() +  "/" + PLIST_PROPERTOES_FILE_NAME);

				byte data[] = new byte[1024];

				long total = 0;

				while ((count = input.read(data)) != -1) {
					total += count;
					publishProgress(""+(int)((total*100)/lenghtOfFile));
					output.write(data, 0, count);
				}

				output.flush();
				output.close();
				input.close();
			} catch (Exception e) {}
			return null;
		}
		
		protected void onProgressUpdate(String... progress) {
			
		}

		@Override
		protected void onPostExecute(String unused) {
			
			String pListFile = getFilesDir() + "/" + PLIST_PROPERTOES_FILE_NAME;
			
			File file = new File(pListFile);
			if (file.exists()) {
				parsePList();
				startLoginActivity();
			}
			else {
				
			}
			
		}
	}
}
