package com.learnnet.hit.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.learnnet.hit.MyApplication;
import com.learnnet.hit.MyApplication.TrackerName;
import com.learnnet.hit.R;
import com.learnnet.hit.Adapters.InboxAdapter;
import com.learnnet.hit.Interface.MyInterface;
import com.learnnet.hit.Objects.InboxData;
import com.learnnet.hit.Utils.GlobalDefs;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class Inbox extends ActionBarActivity implements GlobalDefs, MyInterface {

	// =================================================
	// FIELDS
	// =================================================

	private MyApplication mApp;
	private ListView mInboxListView;
	private InboxAdapter mInboxAdapter;
	private List<InboxData> mInboxData;
	private ActionBar mActionBar;
	private TextView actionBarTitle;
	private ArrayList<String> mSelectedMessages;
	private String messagesJson;
	private String getMessagesUrl;
	private String deleteMessagesUrl;
	private ProgressDialog spinnerDialog;
	private String id;
	private String branch;
	private String deleteResponse;

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	@SuppressLint("InflateParams")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inbox);

		this.mApp = (MyApplication)getApplication();
		
		mApp.getTracker(TrackerName.APP_TRACKER);

		// Inflate your custom action bar layout
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar_centered_text, null);

		// Set up your ActionBar
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setCustomView(actionBarLayout);

		// Set up your ActionBar
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayUseLogoEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);

		// Set ActionBar's text
		actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
		actionBarTitle.setText(getResources().getString(R.string.my_system_messages));

		// Initializing variables
		mInboxData = new ArrayList<InboxData>();
		mInboxListView = (ListView) findViewById(R.id.lvInboxListView);
		mInboxListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				// Clear all check boxes
				deselectAll();
				
				// Open the message
				InboxData msg = mInboxAdapter.getItem(arg2);
				Intent i = new Intent(Inbox.this, InboxExpand.class);
				i.putExtra("title", msg.title);
				i.putExtra("date", msg.date);
				i.putExtra("body", msg.body);
				startActivity(i);
			}			
		});

		mSelectedMessages = new ArrayList<String>();
		
		// Getting messages from server
		id = mApp.mPrefs.getString(PREF_USER_NAME, "");
		branch = getPropertiesString(PROP_BRANCH_ID);
		getMessagesUrl = SERVER_GET_MESSAGES;
		getMessagesUrl += "?id=" + id + "&branch=" + branch;
		new GetMessagesTask().execute(getMessagesUrl);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.inbox_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.delete:
			deleteMessages();
			break;
		case R.id.selectAll:
			selectAll();
			break;
		case R.id.deselectAll:
			deselectAll();
			break;
		}
		return true;
	}

	@Override
	public boolean isNetworkConnected() {
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			return (String) mApp.mPropertiesSpecific.get(key);
		}
		else {
			return null;
		}
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	

	// =================================================
	// CLASS LOGIC
	// =================================================
	
	private void resetMessages() {
		mSelectedMessages.clear();
		mInboxAdapter.clear();
		new GetMessagesTask().execute(getMessagesUrl);
	}
	
	private void checkResponse() {
		String dialogMsg;
		try {
			JSONObject responseJson = new JSONObject(deleteResponse);
			switch (responseJson.getInt("status")) {
			case 200:
				dialogMsg = getResources().getString(R.string.my_dialog_msg_deleted);
				break;
			case 400:
				dialogMsg = getResources().getString(R.string.my_dialog_msg_not_deleted);
				break;
			default:
				dialogMsg = getResources().getString(R.string.my_dialog_try_again_later);
				break;
			}
			
		}
		catch (JSONException e) {
			e.printStackTrace();
			dialogMsg = getResources().getString(R.string.my_dialog_json_exception);
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(dialogMsg)
		       .setCancelable(false)
		       .setPositiveButton(getResources().getString(R.string.my_ok), new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	   resetMessages();
		           }
		       });
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void parseMessages() {
		
		try {
			JSONArray messagesJsonArray = new JSONArray(messagesJson);
			for (int i=0; i<messagesJsonArray.length(); i++) {
				JSONObject messageJsobObject = messagesJsonArray.getJSONObject(i);
				InboxData temp = new InboxData(messageJsobObject.getString("pkId"),
						messageJsobObject.getString("title"),
						messageJsobObject.getString("msg"),
						messageJsobObject.getString("date"));
				mInboxData.add(temp);
			}
			mInboxAdapter = new InboxAdapter(Inbox.this, mInboxData);
			mInboxListView.setAdapter(mInboxAdapter);

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void deleteMessages() {
		deleteMessagesUrl = SERVER_DELETE_MESSAGES + "?pkid=";
		for (InboxData data : mInboxData) {
			if (data.isSelected) {
				if (!mSelectedMessages.contains(data.id)) {
					mSelectedMessages.add(data.id);
				}
			}
			else {
				mSelectedMessages.remove(data.id);
			}
		}
		
		if (!mSelectedMessages.isEmpty()) {
			
			for (String id : mSelectedMessages) {
				deleteMessagesUrl += id + ",";
			}
			if (deleteMessagesUrl.endsWith(",")) {
				deleteMessagesUrl = deleteMessagesUrl.substring(0, deleteMessagesUrl.length()-1);
			}
			deleteMessagesUrl += "&branch=" + branch;

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.my_dialog_sure_to_del_msg))
			.setCancelable(false)
			.setNegativeButton(getResources().getString(R.string.my_cancel), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// Nothing
				}
			})
			.setPositiveButton(getResources().getString(R.string.my_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					new DeleteMessagesTask().execute(deleteMessagesUrl);
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
		
		else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.my_dialog_select_before_del))
			.setCancelable(false)
			.setPositiveButton(getResources().getString(R.string.my_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					// Nothing
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
	}

	private void selectAll() {
		for ( int i=0; i< mInboxAdapter.getCount(); i++ ) {
			RelativeLayout itemLayout = (RelativeLayout) mInboxListView.getChildAt(i);
			CheckBox cb = (CheckBox) itemLayout.findViewById(R.id.message_checkbox);
			cb.setChecked(true);
		}
	}

	private void deselectAll() {
		for ( int i=0; i< mInboxAdapter.getCount(); i++ ) {
			RelativeLayout itemLayout = (RelativeLayout) mInboxListView.getChildAt(i);
			CheckBox cb = (CheckBox) itemLayout.findViewById(R.id.message_checkbox);
			cb.setChecked(false);
		}
	}

	private void displaySpinner() {
		spinnerDialog = ProgressDialog.show(this, "",
				getString(R.string.my_wait), true);
	}

	private void hideSpinner() {
		spinnerDialog.cancel();
	}

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	// =================================================
	// INNER CLASSES
	// =================================================

	private class GetMessagesTask extends AsyncTask<String, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			displaySpinner();
		}

		@Override
		protected Void doInBackground(String... arg0) {
			try {

				HttpParams params = new BasicHttpParams();
				HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params, "UTF-8");

				HttpClient httpclient = new DefaultHttpClient(params);

				// Prepare a request object
				HttpGet httpget = new HttpGet(arg0[0]); // arg0[0] - get messages url 

				// Execute the request
				HttpResponse response;

				response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();

				if (entity != null) {
					String result = EntityUtils.toString(entity, HTTP.UTF_8);
					messagesJson = result;
				}

			} catch (Exception e) {
				Log.e(TAG + " Exception", e.toString());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			hideSpinner();
			parseMessages();
		}
	}

	private class DeleteMessagesTask extends AsyncTask<String, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			displaySpinner();
		}

		@Override
		protected Void doInBackground(String... arg0) {
			try {

				HttpParams params = new BasicHttpParams();
				HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params, "UTF-8");

				HttpClient httpclient = new DefaultHttpClient(params);

				// Prepare a request object
				HttpGet httpget = new HttpGet(arg0[0]); // arg0[0] - get messages url 

				// Execute the request
				HttpResponse response;

				response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();

				if (entity != null) {
					String result = EntityUtils.toString(entity, HTTP.UTF_8);
					deleteResponse = result;
				}

			} catch (Exception e) {
				Log.e(TAG + " Exception", e.toString());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			hideSpinner();
			checkResponse();
		}
	}

}