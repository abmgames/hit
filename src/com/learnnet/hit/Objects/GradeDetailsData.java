package com.learnnet.hit.Objects;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class GradeDetailsData {
	
	public String name = "";
	public String semester = "";
	public String weight = "";
	public String grade = "";
	
}