package com.learnnet.hit.Objects;

import android.annotation.SuppressLint;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
@SuppressWarnings("serial")
public class ScheduleData implements Comparable<Object>,Serializable{
	
	public String SUBJECT = "";
	public String BEGINTIME = "";
	public String ENDTIME  = "";
	public String RESOURCEDATE = "";
	public String LECTURERNAME = "";
	public String ROOMNAME  = "";
	
	@SuppressLint("SimpleDateFormat")
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		Date thisVal ;
		Date anotherVal;
		try
		{
			String begDate = this.RESOURCEDATE + this.BEGINTIME.substring(
					this.BEGINTIME.indexOf(" "), this.BEGINTIME.length() - 3);
			
			String endDate = ((ScheduleData)arg0).RESOURCEDATE + " " + ((ScheduleData)arg0).BEGINTIME.substring(
					((ScheduleData)arg0).BEGINTIME.indexOf(" "), ((ScheduleData)arg0).BEGINTIME.length() - 3);
			
			SimpleDateFormat curFormater = new SimpleDateFormat(
					"dd/MM/yyyy HH:mm");
			
			
			
			thisVal = curFormater.parse(begDate);
			anotherVal = curFormater.parse(endDate);
		}
		catch (Exception e) {
			thisVal = new Date();
			anotherVal = new Date();
		}
		if(thisVal.compareTo(anotherVal)>0)
		{
			return 1;
		}
		else if(thisVal.compareTo(anotherVal)<0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
		
	}
	
}
