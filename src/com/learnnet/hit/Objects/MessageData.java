package com.learnnet.hit.Objects;

import java.util.Date;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MessageData implements Comparable<Object>{
	
	private String MSG_TITLE = "";
	private String MSG_DTUPD = "";
	private String MSG_HTML  = "";
	
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		try {
			String temp1 = this.MSG_DTUPD.substring(2, 12);
			String temp2 = ((MessageData) arg0).MSG_DTUPD.substring(2, 12);

			@SuppressWarnings("deprecation")
			Date thisVal = new Date(temp1 + " 1:00:00 AM");
			@SuppressWarnings("deprecation")
			Date anotherVal = new Date(temp2 + " 1:00:00 AM");

			if (thisVal.compareTo(anotherVal) < 0) {
				return 1;
			} else if (thisVal.compareTo(anotherVal) > 0) {
				return -1;
			} else {
				return 0;
			}

		} catch (Exception ex) {

			return 0;
		}

	}

	public String compareTo(MessageData md) {

		String thisVal = this.MSG_DTUPD;

		String anotherVal = md.MSG_DTUPD;

		if (thisVal.compareTo(anotherVal) > 0) {
			return thisVal;
		} else {
			return anotherVal;
		}

	}
	
	/*public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		Date thisVal = new Date(this.MSG_DTUPD);
		Date anotherVal = new Date(((messageData)arg0).MSG_DTUPD);
 
		if(thisVal.compareTo(anotherVal)>0)
		{
			return -1;
		}
		else if(thisVal.compareTo(anotherVal)<0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
		
	}
	
	
	public String compareTo(messageData md) {
		 
		String thisVal = this.MSG_DTUPD;
 
		String anotherVal = md.MSG_DTUPD;
 
		if(thisVal.compareTo(anotherVal)>0)
		{
			return thisVal;
		}
		else
		{
			return anotherVal;
		}

	}*/
	
	public void setTITLE(String TITLE)
	{
		this.MSG_TITLE = TITLE;
	}
	
	public void setDTUPD(String DTUPD)
	{
		this.MSG_DTUPD = DTUPD;
	}
	
	public void setHTML(String HTML)
	{
		this.MSG_HTML = HTML;
	}
	
	public String getTITLE()
	{
		return MSG_TITLE;
	}
	
	public String getDTUPD()
	{
		return MSG_DTUPD;
	}
	
	public String getHTML()
	{
		return MSG_HTML;
	}

	

}
