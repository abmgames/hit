package com.learnnet.hit.Objects;

import org.xml.sax.helpers.DefaultHandler;

public class TeacherLessonData extends DefaultHandler {
    public String ENDTIME = "";
    public String EVENTID = "";
    public String LOCATION = "";
    public String STARTTIME = "";
    public String SUBJECT = "";
    public String THEDATE = "";
}
