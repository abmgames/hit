package com.learnnet.hit.Objects;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class RequestData {
	
	public String name = "";
	public String date = "";
	public String subject = "";
	public String status = "";
	public String dateAnswered = "";
	public String topic = "";
	public String body = "";
	public String replyBody = "";
	
}
