package com.learnnet.hit.Objects;

import android.annotation.SuppressLint;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
@SuppressWarnings("serial")
public class ExamData implements Comparable<Object>, Serializable {

	public String EXAM_SEMESTER = "";
	public String EXAM_NUMBER = "";
	public String EXAM_DATE_INFO = "";
	public String EXAM_TIME_INFO = "";
	public String EXAM_HEB_DATE = "";
	public String COURSE_DESCRIPTION = "";
	public String TASK_DESCRIPTION = "";
	public String TEACHER_NAME = "";
	public String ROOM_NAME = "";
	public String EXPOSURE_INFO = "";
	public String EXPIRED = "";
	public String KRS_MIS_KVUZA = "";
	public String SHLOHA = "";
	public String STATUS_DESCRIPTION = "";

	@SuppressLint("SimpleDateFormat")
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		try {
			String temp1 = this.EXAM_DATE_INFO.substring(2, 12);
			String temp2 = ((ExamData) arg0).EXAM_DATE_INFO.substring(2, 12);

			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

			// See if we can parse the output of Date.toString()

			Date thisVal =format.parse(temp1);
			Date anotherVal =format.parse(temp2);;

			if (thisVal.compareTo(anotherVal) < 0) {
				return -1;
			} else if (thisVal.compareTo(anotherVal) > 0) {
				return 1;
			} else {
				return 0;
			}

		} catch (Exception ex) {

			return 0;
		}

	}

	public String compareTo(ExamData md) {

		String thisVal = this.EXAM_DATE_INFO;

		String anotherVal = md.EXAM_DATE_INFO;

		if (thisVal.compareTo(anotherVal) > 0) {
			return thisVal;
		} else {
			return anotherVal;
		}

	}

	/*
	 * public int compareTo(Object arg0) { // TODO Auto-generated method stub
	 * 
	 * String thisVal = this.EXAM_NUMBER;
	 * 
	 * String anotherVal = ((examData) arg0).EXAM_NUMBER;
	 * 
	 * try { if (thisVal.compareTo(anotherVal) < 0) { return -1; } else if
	 * (thisVal.compareTo(anotherVal) > 0) { return 1; } else { return 1; } }
	 * catch (Exception ex) {
	 * 
	 * return 1; }
	 * 
	 * }
	 * 
	 * public String compareTo(examData md) {
	 * 
	 * String thisVal = this.EXAM_NUMBER;
	 * 
	 * String anotherVal = md.EXAM_NUMBER;
	 * 
	 * if (thisVal.compareTo(anotherVal) > 0) { return thisVal; } else { return
	 * anotherVal; }
	 * 
	 * }
	 */

}
