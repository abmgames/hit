package com.learnnet.hit.Objects;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class TeacherScheduleData {
	
	public String COURSENAME = "";
	public String BEGINTIME = "";
	public String ENDTIME  = "";
	public String LOCATION  = "";
	public String DAY = "";
	public String NUMOFSTUDENTS = "";
	
}
