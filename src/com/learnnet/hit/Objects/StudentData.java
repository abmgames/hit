package com.learnnet.hit.Objects;

import android.graphics.Bitmap;

public class StudentData {

    public String STUDENTNAME = "";
    public String STUDENTID = "";
    public String STUDENTACADEMICYEAR = "";
    public String STUDENTINFO = "";
    public String MEETINGID = "";
    public String ADMISSION_STATUS = "";
    public Bitmap IMAGE = null;

}
