package com.learnnet.hit.Objects;

import java.util.Date;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class GradeData implements Comparable<Object>{
	
	public String courseNumber = "";
	public String courseName = "";
	public String courseId  = "";
	public String semesterAGrade = "";
	public String semesterBGrade = "";
	public String semesterKaitzGrade  = "";
	public String finalGrade = "";
	public String credits = "";
	public String verbalGrade = "";
	
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		try {
			String temp1 = this.semesterKaitzGrade.substring(2, 12);
			String temp2 = ((GradeData) arg0).semesterKaitzGrade.substring(2, 12);

			@SuppressWarnings("deprecation")
			Date thisVal = new Date(temp1 + " 1:00:00 AM");
			@SuppressWarnings("deprecation")
			Date anotherVal = new Date(temp2 + " 1:00:00 AM");

			if (thisVal.compareTo(anotherVal) < 0) {
				return 1;
			} else if (thisVal.compareTo(anotherVal) > 0) {
				return -1;
			} else {
				return 0;
			}

		} catch (Exception ex) {

			return 0;
		}
		
	}
	
	
	public String compareTo(GradeData md) {
		 
		String thisVal = this.semesterKaitzGrade;

		String anotherVal = md.semesterKaitzGrade;

		if (thisVal.compareTo(anotherVal) > 0) {
			return thisVal;
		} else {
			return anotherVal;
		}

	}
	
}

